{
    "loader" : "tileset",
    "pre_load": true,
    "properties" : {
        "name" : "Blocks",
        "tile_width": 16,
        "tile_height": 16,
        "tile_settings": {
            "1" : {"name":"grass"},
            "2" : {"name":"stone"},
            "3" : {"name":"sand"},
            "4" : {"name":"water"},
            "5" : {"name":"soil"}
        }
    }
}