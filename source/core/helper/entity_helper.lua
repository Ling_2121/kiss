
local entity_helper = {}

function entity_helper.writeEntityToDataFile(data_file,entity)
    local type_name = entity.type_name
    local constructor = core.Resources:getFromGroup("@EntityConstructors",type_name)
    if constructor == nil then return end
    local serialize = constructor.serialize
    
    --写入实体类型
    data_file:writeString(type_name)

    for i,comp_name in ipairs(serialize) do
        local comp = entity:getComponent(comp_name)
        assert(comp ~= nil,string.format("实体写入错误，此实体不存在%s组件",comp_name))

        local comp_constructor = core.Resources:getFromGroup("@ComponentConstructors",comp_name)
        assert(comp_constructor ~= nil,string.format("实体写入错误，找不到%s组件的构造器",comp_name))

        local comp_serializes = comp_constructor.serialize
        assert(comp_serializes ~= nil,string.format("实体写入错误，需要有serializes属性",comp_name))

        --写入组件类型
        data_file:writeString(comp_name)

        for i,data_key in ipairs(comp_serializes) do
            local data = comp[data_key]
            assert(data ~= nil,string.format("实体写入错误，找不到组件数据(key : %s)",data_key))
            data_file:write(data)
        end
    end
end

function entity_helper.readEntityFromDataFile(data_file,entity)
    
end

return entity_helper