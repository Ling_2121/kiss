return function(type_name,t)
    t.__component_constructor__ = true
    t.__is_constructor__ = true
    t.type_name = type_name
    t.serialize = t.serialize or nil --如果为table则在table的数组部分定义序列数据的排布
    t.create = function(self,...)
        local c = {__comp_name__ = type_name,entity = nil}
        t:make(c,...)
        return c
    end

    return setmetatable(t,{__call = t.create})
end