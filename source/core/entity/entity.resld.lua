return {
    name = "entity_component",

    type_tag = "entity_component",
    extension = {
        "_entity.lua",
        "_component.lua",
    },
    pre_load_all = true,

    load = function(res,path,setting)
        if res.item ~= nil then return end;
        local c = loadstring(love.filesystem.read(path))()
        local name = c.type_name
        res.item = c
        if c.__component_constructor__ then
            core.Resources:groupAdd("@ComponentConstructors",name,res)
            
        end
        if c.__entity_constructor__ then
            core.Resources:groupAdd("@EntityConstructors",name,res)
        end
    end;

    get = function(res,path)
        return res.item
    end
}