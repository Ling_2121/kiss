local SandboxRegionComponent = require"source/component/sandbox/sandbox_region_component"

--[[
    单区块数据结构
    x : int32
    y : int32
    tile_dict : dict<string,{tileset:{tile ...} ...}>
    tile_layer_count : int32
    tiles : carray[tile_layer_count][x * y]
    objects : dict<string,dict>
--]]

return core.EntityCountructor("SandboxRegion",{
    make = function(_,self,x,y,width,height,tile_size)
        self:addComponent(SandboxRegionComponent(x,y,width,height,tile_size))
    end
})