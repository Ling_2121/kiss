return core.EntityCountructor("SandboxStorageManager",{
    make = function(_,self,entity,write_f,read_f)
        self.entity = entity
        self.write = write_f or function() return {} end
        self.read = read_f or function(data) end 
    end
})